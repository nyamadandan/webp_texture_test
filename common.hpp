#pragma once

//GLFW
#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#define GLFW_INCLUDE_ES3
#include <GLFW/glfw3.h>
#else
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#endif

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <imgui.h>
#include "imgui_impl_glfw_gl3.h"

// WebP
#include <webp/decode.h>

// stdio
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// Chrono
#include <chrono>

// File
#include <fcntl.h>
#include <sys/stat.h>

#ifdef _WIN32
#include <io.h>
#define open _open
#define read _read
#define close _close
#else
#include <unistd.h>
#endif

#include <crn_defs.h>