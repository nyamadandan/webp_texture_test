#include "application.hpp"

void update(void *arg)
{
    reinterpret_cast<Application *>(arg)->render();
}

int main(void)
{
    Application* renderer = new Application();

    if(!renderer->initialize())
    {
        return -1;
    }

#ifndef __EMSCRIPTEN__
    glfwSwapInterval(1);

    while (!glfwWindowShouldClose(renderer->getWindow()))
    {
        update(renderer);
    }

    renderer->finalize();

    return 0;
#else
    emscripten_set_main_loop_arg(update, renderer, 0, 0);
    glfwSwapInterval(1);
#endif
}
