#ifndef __SHADER_UTILS_H__
#define __SHADER_UTILS_H__

#include "common.hpp"

extern const char* const GlslVersion;

int checkLinked(unsigned int program);
int checkCompiled(unsigned int shader);

#endif