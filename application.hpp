#pragma once

#include "common.hpp"
#include "app_log.hpp"
#include "shader_utils.hpp"
#include "image_renderer.hpp"
#include "webp_renderer.hpp"
#include "webp_rgb_renderer.hpp"
#include "crunch.hpp"

class Application
{
  private:
    GLuint vao;

    GLFWwindow *window;
    WebpRenderer* pYuvRenderer;
    WebpRgbRenderer* pRgbRenderer;

    IImageRenderer* pWebpRenderer;
    IImageRendererTexture *pImageTexture;

  public:
    Application();
    ~Application();

    GLFWwindow *getWindow();

    bool initialize();
    void finalize();
    void render();
};
