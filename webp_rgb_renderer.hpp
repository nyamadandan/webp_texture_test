#pragma once

#include "common.hpp"
#include "shader_utils.hpp"
#include "image_renderer.hpp"

class WebpRgbRenderer : public IImageRenderer
{
private:
    GLuint programHandle;

    GLint uTexRgb;

    GLint uMatModel;
    GLint uMatView;
    GLint uMatProj;

public:
    static const int PositionLocation = 0;
    static const int PositionSize = 3;

    WebpRgbRenderer();

    void initialize();
    IImageRendererTexture *load(const char *fileName);
    void useProgram();
    void setModelViewProj(const glm::mat4x4 &model, const glm::mat4x4 &view, const glm::mat4x4 &proj);
    void bindTextures(IImageRendererTexture *imageTexture);
};
