#pragma once
#include "common.hpp"

class AppLog
{
private:
    ImGuiTextBuffer Buf;
    ImGuiTextFilter Filter;
    ImVector<int> LineOffsets;        // Index to lines offset

public:
    bool ScrollToBottom;
    void Clear();
    void AddLog(const char* fmt, ...);
    void Draw(const char* title, bool* p_open = NULL);
};
