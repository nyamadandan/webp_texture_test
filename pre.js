Module['preRun'] = function () {
  FS.createFolder(
      '/', // 親フォルダの指定
      'assets', // フォルダ名
      true, // 読み込み許可
      false // 書き込み許可(今回の例はfalseでもよさげ)
  );
  FS.createPreloadedFile(
      '/assets', // 親フォルダの指定
      'picture0.webp', // ソース中でのファイル名
      '../assets/picture0.webp', // httpでアクセスする際のURLを指定
      true, // 読み込み許可
      false // 書き込み許可
  );
  FS.createPreloadedFile(
      '/assets', // 親フォルダの指定
      'picture1.webp', // ソース中でのファイル名
      '../assets/picture1.webp', // httpでアクセスする際のURLを指定
      true, // 読み込み許可
      false // 書き込み許可
  );
  FS.createPreloadedFile(
      '/assets', // 親フォルダの指定
      'picture2.webp', // ソース中でのファイル名
      '../assets/picture2.webp', // httpでアクセスする際のURLを指定
      true, // 読み込み許可
      false // 書き込み許可
  );
  FS.createPreloadedFile(
      '/assets', // 親フォルダの指定
      'Roboto-Regular.ttf', // ソース中でのファイル名
      '../assets/Roboto-Regular.ttf', // httpでアクセスする際のURLを指定
      true, // 読み込み許可
      false // 書き込み許可
  );
};