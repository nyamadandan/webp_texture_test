
#include "webp_rgb_renderer.hpp"

static const char* const VsSource =
    "layout (location = 0) in vec3 aPosition;\n"
    "layout (location = 1) in vec2 aUv;\n"
    "\n"
    "out vec2 uv;\n"
    "\n"
    "uniform mat4  matProj;\n"
    "uniform mat4  matView;\n"
    "uniform mat4  matModel;\n"
    "\n"
    "void main(void){\n"
    "  uv = aUv;\n"
    "  gl_Position = matProj * matView * matModel * vec4(aPosition, 1.0);\n"
    "}";

static const char* const FsSource =
    "precision mediump float;\n"
    "\n"
    "in vec2 uv;\n"
    "\n"
    "uniform sampler2D texRgb;\n"
    "\n"
    "out vec4 fragColor;\n"
    "\n"
    "void main(void){\n"
    "  vec2 p = uv;\n"
    "  vec3 rgb = texture(texRgb, p).rgb;\n"
    "  fragColor = vec4(rgb, 1.0);\n"
    "}";

class WebpRgbImageRendererTexture : public IImageRendererTexture
{
private:
    int32_t width;
    int32_t height;
    GLuint textures[2];

public:
    const GLuint *getTextures();
    int32_t getWidth();
    int32_t getHeight();

    GLuint getRGB();

    WebpRgbImageRendererTexture(int32_t width, int32_t height, GLuint texRgb);
    ~WebpRgbImageRendererTexture();

    void unload();
};

WebpRgbImageRendererTexture::WebpRgbImageRendererTexture(int32_t width, int32_t height, GLuint texRgb)
{
    this->width = width;
    this->height = height;
    this->textures[0] = texRgb;
}

int32_t WebpRgbImageRendererTexture::getWidth()
{
    return this->width;
}

int32_t WebpRgbImageRendererTexture::getHeight()
{
    return this->height;
}

GLuint WebpRgbImageRendererTexture::getRGB()
{
    return this->textures[0];
}

WebpRgbImageRendererTexture::~WebpRgbImageRendererTexture()
{
}

const GLuint *WebpRgbImageRendererTexture::getTextures()
{
    return this->textures;
}

void WebpRgbImageRendererTexture::unload()
{
    glDeleteTextures(1, this->textures);
    this->textures[0] = 0;
}

WebpRgbRenderer::WebpRgbRenderer()
{
}

void WebpRgbRenderer::initialize()
{
    const char* const vsSources[2] = {GlslVersion, VsSource};
    const char* const fsSources[2] = {GlslVersion, FsSource};

    const GLuint handleVS = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(handleVS, 2, vsSources, NULL);
    glCompileShader(handleVS);
    assert(checkCompiled(handleVS));

    const GLuint handleFS = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(handleFS, 2, fsSources, NULL);
    glCompileShader(handleFS);
    assert(checkCompiled(handleFS));

    this->programHandle = glCreateProgram();
    glAttachShader(this->programHandle, handleVS);
    glAttachShader(this->programHandle, handleFS);
    glLinkProgram(this->programHandle);
    assert(checkLinked(this->programHandle));

    glDeleteShader(handleVS);
    glDeleteShader(handleFS);

    this->uTexRgb = glGetUniformLocation(this->programHandle, "texRgb");
    this->uMatModel = glGetUniformLocation(this->programHandle, "matModel");
    this->uMatView = glGetUniformLocation(this->programHandle, "matView");
    this->uMatProj = glGetUniformLocation(this->programHandle, "matProj");
}

IImageRendererTexture *WebpRgbRenderer::load(const char *fileName)
{
    struct stat stbuf;
    GLuint texRgb;

    int fd = open(fileName, O_RDONLY);
    assert(fd >= 0);

    fstat(fd, &stbuf);

    uint32_t fileSize = stbuf.st_size;
    uint8_t *buf = static_cast<uint8_t *>(malloc(sizeof(uint8_t) * fileSize));
    read(fd, buf, fileSize);
    close(fd);
    fd = -1;

    int width, height, stride, uvStride;
    uint8_t *rgbChannel;

    rgbChannel = WebPDecodeRGB(buf, fileSize, &width, &height);

    glGenTextures(1, &texRgb);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, texRgb);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rgbChannel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    free(rgbChannel);
    free(buf);

    return new WebpRgbImageRendererTexture(width, height, texRgb);
}

void WebpRgbRenderer::useProgram()
{
    glUseProgram(this->programHandle);

}

void WebpRgbRenderer::setModelViewProj(const glm::mat4x4 &model, const glm::mat4x4 &view, const glm::mat4x4 &proj)
{
    if (this->uMatModel >= 0) {
        glUniformMatrix4fv(this->uMatModel, 1, false, glm::value_ptr(model));
    }

    if (this->uMatView >= 0) {
        glUniformMatrix4fv(this->uMatView, 1, false, glm::value_ptr(view));
    }

    if (this->uMatProj >= 0) {
        glUniformMatrix4fv(this->uMatProj, 1, false, glm::value_ptr(proj));
    }
}

void WebpRgbRenderer::bindTextures(IImageRendererTexture *imageTexture)
{
    WebpRgbImageRendererTexture *webpTexture = dynamic_cast<WebpRgbImageRendererTexture*>(imageTexture);

    if(this->uTexRgb >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, webpTexture->getRGB());
        glUniform1i(this->uTexRgb, 0);
    }
}
