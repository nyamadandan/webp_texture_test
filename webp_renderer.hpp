#pragma once

#include "common.hpp"
#include "shader_utils.hpp"
#include "image_renderer.hpp"

class WebpRenderer : public IImageRenderer
{
private:
    GLuint programHandle;

    GLint uTexLuma;
    GLint uTexChroma;

    GLint uMatModel;
    GLint uMatView;
    GLint uMatProj;

public:
    WebpRenderer();

    void initialize();
    IImageRendererTexture *load(const char *fileName);
    void useProgram();
    void setModelViewProj(const glm::mat4x4 &model, const glm::mat4x4 &view, const glm::mat4x4 &proj);
    void bindTextures(IImageRendererTexture *imageTexture);
};
