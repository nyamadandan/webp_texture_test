#include "webp_renderer.hpp"

static const char* const VsSource =
    "layout (location = 0) in vec3 aPosition;\n"
    "layout (location = 1) in vec2 aUv;\n"
    "\n"
    "out vec2 uv;\n"
    "\n"
    "uniform mat4  matProj;\n"
    "uniform mat4  matView;\n"
    "uniform mat4  matModel;\n"
    "\n"
    "void main(void){\n"
    "  uv = aUv;\n"
    "  gl_Position = matProj * matView * matModel * vec4(aPosition, 1.0);\n"
    "}";

static const char* const FsSource =
    "precision mediump float;\n"
    "\n"
    "in vec2 uv;\n"
    "\n"
    "uniform sampler2D texLuma;\n"
    "uniform sampler2D texChroma;\n"
    "\n"
    "out vec4 fragColor;\n"
    "\n"
    "void main(void){\n"
    "  vec2 p = uv;\n"
    "\n"
    "  float chY = 1.164 * (texture(texLuma, p).x - 16.0 / 255.0);\n"
    "  vec2 chUV = texture(texChroma, p).xy - 128.0 / 255.0;\n"
    "  float chU = chUV.x;\n"
    "  float chV = chUV.y;\n"
    "\n"
    "  vec3 rgb = vec3(chY + 1.596 * chV, chY  - 0.391 * chU - 0.813 * chV, chY + 2.018 * chU);\n"
    "\n"
    "  fragColor = vec4(rgb, 1.0);\n"
    "}";

class WebpImageRendererTexture : public IImageRendererTexture
{
private:
    int32_t width;
    int32_t height;
    GLuint textures[2];

public:
    int32_t getWidth();
    int32_t getHeight();

    GLuint getLuma();
    GLuint getChroma();
    const GLuint *getTextures();

    WebpImageRendererTexture(int32_t width, int32_t height, GLuint texLuma, GLuint texChroma);
    ~WebpImageRendererTexture();

    void unload();
};

WebpImageRendererTexture::WebpImageRendererTexture(int32_t width, int32_t height, GLuint texLuma, GLuint texChroma)
{
    this->width = width;
    this->height = height;
    this->textures[0] = texLuma;
    this->textures[1] = texChroma;
}

WebpImageRendererTexture::~WebpImageRendererTexture()
{
}

const GLuint *WebpImageRendererTexture::getTextures()
{
    return this->textures;
}

int32_t WebpImageRendererTexture::getWidth()
{
    return this->width;
}

int32_t WebpImageRendererTexture::getHeight()
{
    return this->height;
}

GLuint WebpImageRendererTexture::getLuma()
{
    return this->textures[0];
}

GLuint WebpImageRendererTexture::getChroma()
{
    return this->textures[1];
}

void WebpImageRendererTexture::unload()
{
    glDeleteTextures(2, this->textures);
    this->textures[0] = 0;
    this->textures[1] = 0;
}

WebpRenderer::WebpRenderer()
{
}

void WebpRenderer::initialize()
{
    const char* const vsSources[2] = {GlslVersion, VsSource};
    const char* const fsSources[2] = {GlslVersion, FsSource};

    const GLuint handleVS = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(handleVS, 2, vsSources, NULL);
    glCompileShader(handleVS);
    assert(checkCompiled(handleVS));

    const GLuint handleFS = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(handleFS, 2, fsSources, NULL);
    glCompileShader(handleFS);
    assert(checkCompiled(handleFS));

    this->programHandle = glCreateProgram();
    glAttachShader(this->programHandle, handleVS);
    glAttachShader(this->programHandle, handleFS);
    glLinkProgram(this->programHandle);
    assert(checkLinked(this->programHandle));

    glDeleteShader(handleVS);
    glDeleteShader(handleFS);

    this->uTexLuma = glGetUniformLocation(this->programHandle, "texLuma");
    this->uTexChroma = glGetUniformLocation(this->programHandle, "texChroma");

    this->uMatModel = glGetUniformLocation(this->programHandle, "matModel");
    this->uMatView = glGetUniformLocation(this->programHandle, "matView");
    this->uMatProj = glGetUniformLocation(this->programHandle, "matProj");
}

IImageRendererTexture *WebpRenderer::load(const char *fileName)
{
    struct stat stbuf;

    GLuint texLuma;
    GLuint texChroma;

    int fd = open(fileName, O_RDONLY);
    assert(fd >= 0);

    fstat(fd, &stbuf);

    uint32_t fileSize = stbuf.st_size;
    uint8_t *buf = static_cast<uint8_t *>(malloc(sizeof(uint8_t) * fileSize));
    read(fd, buf, fileSize);
    close(fd);
    fd = -1;

    int width, height, stride, uvStride;
    uint8_t *yChannel, *uChannel, *vChannel;

    yChannel = WebPDecodeYUV(buf, fileSize, &width, &height, &uChannel, &vChannel, &stride, &uvStride);
    glGenTextures(1, &texLuma);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, texLuma);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, yChannel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    int uvWidth = width >> 1;
    int uvHeight = height >> 1;
    uint8_t *uvBuff = static_cast<uint8_t *>(malloc(sizeof(uint8_t) * 2 * uvWidth * uvHeight));
    for (int y = 0; y < uvHeight; y++) {
        for (int x = 0; x < uvWidth; x++) {
            const int iSrc = y * uvWidth + x;
            const int iDstU = iSrc << 1;
            const int iDstV = iDstU + 1;

            uvBuff[iDstU] = uChannel[iSrc];
            uvBuff[iDstV] = vChannel[iSrc];
        }
    }
    glGenTextures(1, &texChroma);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, texChroma);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG8, uvWidth, uvHeight, 0, GL_RG, GL_UNSIGNED_BYTE, uvBuff);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    free(uvBuff);
    free(yChannel);
    free(buf);

    return new WebpImageRendererTexture(width, height, texLuma, texChroma);
}

void WebpRenderer::useProgram()
{
    glUseProgram(this->programHandle);

}

void WebpRenderer::setModelViewProj(const glm::mat4x4 &model, const glm::mat4x4 &view, const glm::mat4x4 &proj)
{
    if (this->uMatModel >= 0) {
        glUniformMatrix4fv(this->uMatModel, 1, false, glm::value_ptr(model));
    }

    if (this->uMatView >= 0) {
        glUniformMatrix4fv(this->uMatView, 1, false, glm::value_ptr(view));
    }

    if (this->uMatProj >= 0) {
        glUniformMatrix4fv(this->uMatProj, 1, false, glm::value_ptr(proj));
    }
}

void WebpRenderer::bindTextures(IImageRendererTexture *imageTexture)
{
    WebpImageRendererTexture *webpTexture = dynamic_cast<WebpImageRendererTexture*>(imageTexture);

    if(this->uTexLuma >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, webpTexture->getLuma());
        glUniform1i(this->uTexLuma, 0);
    }

    if(this->uTexChroma >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, webpTexture->getChroma());
        glUniform1i(this->uTexChroma, 1);
    }
}
