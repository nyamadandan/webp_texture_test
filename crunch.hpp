#ifndef __CRUNCH_HPP__
#define __CRUNCH_HPP__

#include "common.hpp"

unsigned int crn_get_uncompressed_size(void* src, unsigned int src_size, unsigned int level);
void crn_decompress(void* src, unsigned int src_size, void* dst, unsigned int dst_size, unsigned int firstLevel, unsigned int levelCount);
#endif
