#ifndef __IWEBP_RENDERER_HPP__
#define __IWEBP_RENDERER_HPP__

struct IImageRendererTexture
{
    virtual int32_t getWidth() = 0;
    virtual int32_t getHeight() = 0;
    virtual void unload() = 0;
    virtual ~IImageRendererTexture() {}
};

struct IImageRenderer
{
    virtual void initialize() = 0;
    virtual IImageRendererTexture *load(const char *fileName) = 0;
    virtual void useProgram() = 0;
    virtual void setModelViewProj(const glm::mat4x4 &model, const glm::mat4x4 &view, const glm::mat4x4 &proj) = 0;
    virtual void bindTextures(IImageRendererTexture *imageTexture) = 0;
    virtual ~IImageRenderer() {}
};

#endif
