cmake_minimum_required(VERSION 3.1.0)
project(webp_texture_test VERSION 0.1.0)

set(APP_SOURCES
    main.cpp
    application.cpp
    app_log.cpp
    webp_renderer.cpp
    webp_rgb_renderer.cpp
    shader_utils.cpp
    crunch.cpp
)

set(GLAD_SOURCES
    glad/src/glad.c
)

set(IMGUI_SOURCES
    imgui/imgui.cpp
    imgui/imgui_draw.cpp
    imgui/imgui_demo.cpp
    imgui/imgui_widgets.cpp
    imgui_impl_glfw_gl3.cpp
)

if(EMSCRIPTEN)
    set(CMAKE_EXECUTABLE_SUFFIX ".html")
    set(CMAKE_EXE_LINKER_FLAGS "--shell-file ${CMAKE_CURRENT_SOURCE_DIR}/shell.html --pre-js ${CMAKE_CURRENT_SOURCE_DIR}/pre.js -s USE_WEBGL2=1 -s USE_GLFW=3 -s WASM=1 -s ALLOW_MEMORY_GROWTH=1")

    #GLM
    include_directories(./glm)

    # WebP
    add_subdirectory(./libwebp)
    include_directories(./libwebp/src)

    # Imgui
    include_directories(./imgui)

    # Crunch
    include_directories(./crunch/inc)

    add_executable(webp_texture_test ${APP_SOURCES} ${IMGUI_SOURCES})
    add_dependencies(webp_texture_test webp)
    target_link_libraries(webp_texture_test webp)
elseif(MSVC)
    # OpenGL
    find_package(OpenGL REQUIRED)
    include_directories(${OPENGL_INCLUDE_DIRS})

    # glad
    include_directories(./glad/include)

	# Imgui
    include_directories(./imgui)

    # GLFW
    add_subdirectory(./glfw)
    include_directories(./glfw/include)
    include_directories(${CMAKE_CURRENT_BINARY_DIR}/glfw/src)

    #GLM
    include_directories(./glm)

    # WebP
    add_subdirectory(./libwebp)
    include_directories(./libwebp/src)

    # Crunch
    include_directories(./crunch/inc)

    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
    add_executable(webp_texture_test ${APP_SOURCES} ${IMGUI_SOURCES} ${GLAD_SOURCES})
    add_dependencies(webp_texture_test webp glfw)
    target_link_libraries(webp_texture_test ${OPENGL_LIBRARIES} glfw webp)
else(APPLE)
    # OpenGL
    find_package(OpenGL REQUIRED)
    include_directories(${OPENGL_INCLUDE_DIRS})

    # glad
    include_directories(./glad/include)

	# Imgui
    include_directories(./imgui)

    # GLFW
    add_subdirectory(./glfw)
    include_directories(./glfw/include)
    include_directories(${CMAKE_CURRENT_BINARY_DIR}/glfw/src)

    #GLM
    include_directories(./glm)

    # WebP
    add_subdirectory(./libwebp)
    include_directories(./libwebp/src)

    # Crunch
    include_directories(/usr/include/malloc ./crunch/inc)

    add_executable(webp_texture_test ${APP_SOURCES} ${IMGUI_SOURCES} ${GLAD_SOURCES})
    add_dependencies(webp_texture_test webp glfw)
    target_link_libraries(webp_texture_test ${OPENGL_LIBRARIES} glfw webp)
endif()

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
