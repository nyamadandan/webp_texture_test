#include "application.hpp"

static const int PositionLocation = 0;
static const int PositionSize = 3;
static const int UvLocation = 1;
static const int UvSize = 2;

static const GLfloat positions[] = {
    -1.0f, 1.0f, 0.0f,
    1.0f, 1.0f, 0.0f,
    -1.0f, -1.0f, 0.0f,
    1.0f, -1.0f, 0.0f
};

static const GLfloat uvs[] = {
    0.0f, 1.0f,
    1.0f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f
};

static const GLushort indices[] = {
    0, 2, 1,
    1, 2, 3
};

static void readCrunchTest()
{
    const char *fileName = "./assets/picture0.dxtc.crn";
    struct stat stbuf;

    int fd = open(fileName, O_RDONLY);
    if (fd < 0)
    {
        fprintf(stderr, "Load error: %s\n", fileName);
        return;
    }

    fstat(fd, &stbuf);

    uint32_t srcDataSize = stbuf.st_size;
    uint8_t *srcData = static_cast<uint8_t *>(malloc(sizeof(uint8_t) * srcDataSize));
    read(fd, srcData, srcDataSize);
    close(fd);
    fd = -1;

    crnd::crn_texture_info tex_info;
    crnd::crnd_get_texture_info(static_cast<crn_uint8 *>(srcData), srcDataSize, &tex_info);

    unsigned int dstSize = crn_get_uncompressed_size(srcData, srcDataSize, 0);
    uint8_t *dstData = static_cast<uint8_t *>(malloc(sizeof(uint8_t) * dstSize));

    crn_decompress(srcData, srcDataSize, dstData, dstSize, 0, 1);

    free(srcData);
    free(dstData);
}

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Error %d: %s\n", error, description);
}

Application::Application()
{
    this->window = nullptr;

    this->vao = 0;

    this->pYuvRenderer = new WebpRenderer();
    this->pRgbRenderer = new WebpRgbRenderer();

    this->pWebpRenderer = nullptr;
}

Application::~Application()
{
    delete this->pYuvRenderer;
    delete this->pRgbRenderer;
    delete this->pImageTexture;

    this->pYuvRenderer = nullptr;
    this->pRgbRenderer = nullptr;
    this->pWebpRenderer = nullptr;
    this->pImageTexture = nullptr;
}

bool Application::initialize()
{
    // readCrunchTest();

    glfwSetErrorCallback(glfw_error_callback);
    glfwInit();

#ifdef __EMSCRIPTEN__
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#else
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    this->window = glfwCreateWindow(1280, 720, "WebP Texture Test", NULL, NULL);
    assert(this->window != nullptr);
    glfwMakeContextCurrent(this->window);

#ifndef __EMSCRIPTEN__
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
#endif

    ImGui::SetCurrentContext(ImGui::CreateContext());
#ifndef __EMSCRIPTEN__
    ImGui_ImplGlfwGL3_Init(window, true, "#version 150 core");
#else
    ImGui_ImplGlfwGL3_Init(window, true, "#version 300 es");
#endif

    ImGuiIO& io = ImGui::GetIO();
    io.Fonts->AddFontFromFileTTF("./assets/Roboto-Regular.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());

    ImGui::StyleColorsDark();

    this->pYuvRenderer->initialize();
    this->pRgbRenderer->initialize();

    this->pWebpRenderer = this->pYuvRenderer;
    this->pImageTexture = this->pWebpRenderer->load("./assets/picture0.webp");

    GLuint vIndex = 0;
    GLuint vPosition = 0;
    GLuint vUv = 0;

    glGenVertexArrays(1, &this->vao);
    glGenBuffers(1, &vIndex);
    glGenBuffers(1, &vPosition);
    glGenBuffers(1, &vUv);
    glBindVertexArray(this->vao);

    glBindBuffer(GL_ARRAY_BUFFER, vPosition);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 12, positions, GL_STATIC_DRAW);
    glVertexAttribPointer(PositionLocation, PositionSize, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, vUv);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 8, uvs, GL_STATIC_DRAW);
    glVertexAttribPointer(UvLocation, UvSize, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vIndex);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * 6, indices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);

    return true;
}

GLFWwindow *Application::getWindow()
{
    return this->window;
}

void Application::render()
{
    const char* const fileNames[] = {"./assets/picture0.webp", "./assets/picture1.webp", "./assets/picture2.webp"};
    static int32_t currentFile = 0;

    const char* const colorSpaces[] = {"YUV", "RGB"};
    static int32_t currentColorSpace = 0;

    static bool showDemoWindow = false;

    static ImVec4 clearColor = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    static bool showAppLog = false;
    static AppLog appLog;

    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    ImGui_ImplGlfwGL3_NewFrame();

    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::ColorEdit3("clear color", (float *)&clearColor); // Edit 3 floats representing a color

    bool needsReload = false;

    if (ImGui::Combo("color space", &currentColorSpace, colorSpaces, IM_ARRAYSIZE(colorSpaces))) {
        this->pImageTexture->unload();
        needsReload = true;

        switch (currentColorSpace) {
        case 0:
            this->pWebpRenderer = this->pYuvRenderer;
            break;
        case 1:
            this->pWebpRenderer = this->pRgbRenderer;
            break;
        }
    }

    if (ImGui::Combo("image", &currentFile, fileNames, IM_ARRAYSIZE(fileNames))) {
        this->pImageTexture->unload();
        needsReload = true;
    }

    if (needsReload) {
        auto t0 = std::chrono::system_clock::now();
        this->pImageTexture = this->pWebpRenderer->load(fileNames[currentFile]);
        auto t1 = std::chrono::system_clock::now();
        auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
        appLog.AddLog(
            "[%s] %s : %ldms\n",
            colorSpaces[currentColorSpace],
            fileNames[currentFile],
            msec
        );
    }

    if (showAppLog) {
        appLog.Draw("App Log");
    }

    ImGui::Checkbox("Show App Log", &showAppLog);
    // ImGui::Checkbox("Demo Window", &showDemoWindow);      // Edit bools storing our windows open/close state

    float aspectRatio = static_cast<float>(width) / static_cast<float>(height);
    glm::mat4x4 matProj = glm::ortho(-aspectRatio, aspectRatio, -1.0f, 1.0f);
    glm::mat4x4 matView = glm::mat4(1.0f);
    glm::mat4x4 matModel = glm::scale(glm::mat4(1.0f), glm::vec3(static_cast<float>(this->pImageTexture->getWidth()) / static_cast<float>(this->pImageTexture->getHeight()), 1.0f, 1.0f));

    if (showDemoWindow)
    {
        ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver);
        ImGui::ShowDemoWindow(&showDemoWindow);
    }

    glViewport(0, 0, width, height);
    glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    this->pWebpRenderer->useProgram();
    this->pWebpRenderer->bindTextures(this->pImageTexture);
    this->pWebpRenderer->setModelViewProj(matModel, matView, matProj);

    glBindVertexArray(this->vao);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);

    for(GLint error = glGetError(); error; error = glGetError()) {
        fprintf(stderr, "error: Code(0x%04X)\n", error);
    }

    ImGui::Render();
    ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(this->window);
    glfwPollEvents();
}

void Application::finalize()
{
    glfwTerminate();
}
